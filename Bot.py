# -*- coding: utf-8 -*-
"""Sviluppato da Davide Leone - leonedavide[at]protonmail.com - in Python 3.
    Distribuito sotto licenza GNU Affero GPL - Copyright 2018 Davide Leone
    Versione 3.5.3 MoonLight MoonBot 25/08/18

    MoonBot is a Telegram Bot that offers a lot of different instuments.
    Copyright (C) 2018  Davide Leone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

from CONFIG import * #Version 3.5.2
from techlib import * #Version 0.7 

def filtro(msg):
    global antiflood, chat_id, chat_type, mess_id, fields, tim0
    fields = list(msg.keys())
    tim0 = time.time()
    if 'message' in fields: #Si tratta di un bottone
        chat_id = msg['message']['chat']['id']
        chat_type = msg['message']['chat']['type']
        mess_id = msg['message']['message_id']
    else:
        chat_id = msg['chat']['id'] #ID dell'utente
        chat_type = msg['chat']['type'] #Tipo di chat (gruppo o privata)
        mess_id= msg['message_id'] #Identificativo del messaggio
    if chat_id not in list(antiflood.keys()): #Verifica l'esistenza del valore antiflood relativo all'utent
        antiflood[chat_id] = 0
        
    if antiflood[chat_id] < 6: #Numero massimo di messaggi inviabili ogni 5 secondi
        antiflood[chat_id] += 1 #Aggiorna il filtro antiflood
        try: #Avvia il bot vero e proprio. Se ci sono degli errori li valuta
            log(msg)
        except telepot.exception.TooManyRequestsError: #Errore noto: filtro antiflood di Telegram
            if chat_type == 'private':
                bot.sendMessage(chat_id,"⚠ *ERRORE TELEGRAM 400:* avete mandato troppe richieste",'Markdown')
            antiflood[chat_id] = 7
        except Exception as e: 
            if str(e) == str(('Bad Request: message is not modified', 400, {'ok': False, 'error_code': 400, 'description': 'Bad Request: message is not modified'})):
                None #Errore noto. Invia due richieste per modificare allo stesso modo uno stesso messaggio
            elif str(e) == str(('Forbidden: bot was blocked by the user', 403, {'ok': False, 'error_code': 403, 'description': 'Forbidden: bot was blocked by the user'})):
                None #Errore noto. Il bot non riesce a contattare un utente da cui è stato bloccato
            else: #Errore non noto.
                scrivi('moonlogging.txt',(time.ctime()+str(e)+'\n')) #Logging dell'errore su un file apposito
            
    elif antiflood[chat_id] == 6 and chat_type == 'private': #L'utente viene avvisato del filtro
        antiflood[chat_id] = 7 #Viene mandato un solo avviso
        bot.sendMessage(chat_id,"⚠ *FILTRO ANTIFLOOD*\nSta mandando *troppe richieste*. Attenda prima di mandarne di nuove.",parse_mode='Markdown')
    
def log(msg): #Inizio del bot
    global imor, mess_id, imop, ram, chat_id, tipo, command, chat_type, mess_id, fields
    if 'data' in fields:
        tipo = 1 #Si tratta di un bottone
    else:
        tipo = 0 #Si tratta di un testo
        
    ram = scarica(ssd) #Carica tutti i dati
    
    if chat_id in list(ram.keys()): #Utente già registrato
        imop = ram[chat_id] #Recupera il valore imop come copia del valore ram
        imop = parole(ram[chat_id]) #Spacchetta il valore
        imor = imop[0] #Ne recupera il primo elemento e lo definisce imor
        ram[chat_id] = '0' #Il valore ram viene resettato, imop ne resta la copia
    else:
        ram[chat_id], imop, imor = '0', ['0'], '0'

    eccezioni = ['1','8','11','12'] #Comandi in cui si attende un messaggio inoltrato; anche se il testo corrispondesse ad un comando verrebbe valutato correttamente
    
    if tipo == 1:
        click(msg) #Avvia la procedura per i pulsanti
    elif 'forward_date' in fields and esclusioni(imor,eccezioni) == True: #Si tratta di un messaggio inoltrato
        mor(msg)
    else:
        gestisci(msg) #Procede normalmente (controlla che non sia stato mandato un comando generale)

def click(msg): #Comandi basat sui pulsanti
    global chat_id,mess_id,ram,fields
    testo = parole(msg['data']) 
    command = testo[0]
    if len(testo) > 1 and len(msg['data']) > len(command): #Crea un tasto "indietro"
        pos = elimina(msg['data'],len(command)) #Il callback_data del bottone cliccato in precedenza dovrebbe essere strutturato: comando e comando per tornare indietro
        tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='⬅ Indietro',callback_data=(pos))]]))
    
    if command == "recensione":
        ram[chat_id] = '4'
        message = "Siamo felici tu voglia recensire il nostro bot!\nManda ora un messaggio con le tue *critiche*, *idee*, *proposte*.\nGrazie! :)"
        if elimina_caratteri(msg['message']['text'],markdown) != elimina_caratteri(message,markdown):
            bot.editMessageText((chat_id,mess_id),message,'Markdown',True,tastiera)
    else:
        msg['text'] = msg['data']
        fields = list(msg.keys())
        gestisci(msg) #Procede normalmente

    carica(ssd,ram) #Aggiorna i dati
    
def gestisci(msg): #Procedura normale
    global chat_id,mess_id,testo, tipo, tim0, fields
    if 'text' in fields:
        testo = parole(msg['text']) #Il testo viene spezzettato nelle singole parole per recuperare la prima, il comando
        command = (testo)[0].lower() #Il comando viene ridotto in minuscolo per supportare tutte le combinazioni min/maiusc
        testostart = parole(sostituisci(msg['text'],'-',' ')) #Caso dei deeplink 
        if len(testostart)>1 and testostart[0]=="/start":
            testostart.remove('/start')
            testostart[0] = '/'+testostart[0]
            command = testostart[0]
        if estrai(command,-15) == "@moonlightrobot" and len(command) > 15: #Nota. Il comando termina con l'username quando usato nei gruppi con più bot dove non è amministratore
            num = len(command)-15
            command = estrai(command,num)
    else:
        command = "" #Se il file fosse privo di testo (es. foto) non si andrà a cercare alcun comando

    if command == "/start":
        message = "Ciao! Io sono @MoonLightRobot.\nRaccolgo diversi strumenti utili per gli utenti, _come un coltellino svizzero digitale_.\nPuoi lanciare /help per una lista di comandi che supporto.\n\n*Per contattare i miei sviluppatori scrivi a* @DavideLeone"
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)
        
    elif command == "/help": #Manuale per l'utente
        try:
            pagina = abs(int(testo[1])) #Comando /help n, per accedere ad una determinata pagina (anche tramite bottone)
        except:
            pagina = 1 #Comando /help
        
        pagine_totali = 3 #Numero delle pagine

        while pagina > pagine_totali:
            pagina -= 3

        if pagina == 1:
            frecce = ([[InlineKeyboardButton(text=emojize(':information: Informazioni'),callback_data='/info'),
            InlineKeyboardButton(text=emojize(':busts_in_silhouette: Sviluppatore'),url='t.me/DavideLeone')]])
            frecce += ([[InlineKeyboardButton(text='⬅️',callback_data=('/help '+str(pagine_totali))),
                        InlineKeyboardButton(text='➡',callback_data=('/help '+str(pagina+1)))]])
        else:
            frecce = ([[InlineKeyboardButton(text='⬅️',callback_data=('/help '+str(pagina-1))),
                        InlineKeyboardButton(text='➡',callback_data=('/help '+str(pagina+1)))]])

            
        tastiera = InlineKeyboardMarkup(inline_keyboard=(
            [[InlineKeyboardButton(text=emojize(':speaking_head: Feedback'),callback_data=('recensione '+'/help '+str(pagina)))]]+frecce))

        if pagina == 0 or pagina == 1:
            message = "*COMANDI DEL BOT.\nPagina 1 di "+str(pagine_totali)+"""*
I comandi possono essere lanciati anche senza l'argomento fra parentesi.\n
/help [[pagina]] - Naviga attraverso il manuale
/short [[link]] - Accorcia un link
/roll [[numero di dadi]] [[facce]] - Tira uno o più dadi con un qualsiasi numero di facce
/postlink - Ottieni il link del post di un canale
/morse [[testo]] - Trascrivi testi in codice morse
/enigma - Criptazione e decifrazione di un testo con la macchina enigma
/upload - Ottieni un link di condivisione per un file
/pass - Genera una password di 14 caratteri

"""

        if pagina == 2:
            message = "*Pagina 2 di "+str(pagine_totali)+"""*
/spass [[numero]] - Genera una password con caratteri speciali della lunghezza scelta
/toc - Testa o croce
/binario - Trasforma un numero in binario
/getid - Ottieni l'ID Telegram di un altro utente
/myid - Ottieni il tuo ID Telegram
/morsetrad [[testo]] - Trascrive testi dal codice morse
/infomex - Ottieni tutte le informazioni di un messaggio (avanzato)
/conta - Lunghezza di un messaggio
"""
        if pagina == 3:
            message = "*Pagina 3 di "+str(pagine_totali)+"""*
/time - Scopri in che momento è stato inviato per la prima volta un messaggio
/info - Informazioni sul bot
/download - Scarica il codice sorgente
/license - Informazioni sulla licenza
/stats - Statistiche relative al bot
/eliminami - Cancella tutti i dati dell'utente
/globalmessage - Invia un messaggio a tutti gli utenti (comando riservato)
"""
        if tipo == 1:
            if elimina_caratteri(msg['message']['text'],markdown) != elimina_caratteri(message,markdown):
                bot.editMessageText((chat_id,mess_id),message,'Markdown',True,tastiera)
        else:
            bot.sendMessage(chat_id,message,'Markdown',True,reply_markup=tastiera)

    elif command == "/info": #Info sul bot
        message = emojize(':full_moon:')+""" @MoonLightRobot v3.4 è stato sviluppato da @DavideLeone.
Il bot è stato scritto in [Python 3](https://www.python.org/) ed è attualmente hostato su un [Raspberry Pi](https://www.raspberrypi.org/).
Per info sulla licenza di distribuzione usa /license.
Per le statistiche relative al bot puoi utilizzare /stats.
\n@MoonLightRobot conserva solo le informazioni *necessarie* al suo funzionamento.
Le informazioni registrate *non* sono sensibili e sono anonimizzate il più possibile.
Se stai per abbandonare il bot e/o vuoi cancellare tutti i tuoi dati usa /eliminami.
Il bot cancellerà ogni dato a te collegato, non ti contatterà più e non ti conteggerà fra gli utenti registrati."""
        if tipo == 1: #Se si è arrivati qui con un bottone viene modificato il messaggio, altrimenti ne viene inviato un altro
            tastiera = InlineKeyboardMarkup(inline_keyboard=([[InlineKeyboardButton(text='Indietro',callback_data='/help')]]))
            if elimina_caratteri(msg['message']['text'],markdown) != elimina_caratteri(message,markdown):
                bot.editMessageText((chat_id,mess_id),message,'Markdown',True,tastiera)
        else:
            bot.sendMessage(chat_id,message,'Markdown',True,None,mess_id)
            
    elif command == "/license":
        message = """Distribuito sotto licenza [AGPL](https://www.gnu.org/licenses/agpl-3.0.html).
Questo programma è software libero distribuito senza garanzia.
*Copyright 2018 Davide Leone*
\nPer ottenere il codice sorgente lanciate /download.\nPer contatti: leonedavide@protonmail.com o @DavideLeone."""
        bot.sendMessage(chat_id,message,'Markdown',True,None,mess_id)
        
    elif command == "/getid":
        message = "Vuoi ottenere l'ID Telegram di un altro utente? Benissimo.\n*Inoltrami ora un suo messaggio*, ed io provvederò al resto.\nPer scoprire il tuo ID usa invece /myid"
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)
        ram[chat_id] = '1'
        
    elif command == "/morse":
        ram[chat_id] = '2'
        try:
            testo[1]
            mor(msg)
        except:
            message = "Inviami ora la frase che vuoi convertire in *codice morse*."
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)
        
    elif command == "/morsetrad":
        ram[chat_id] = '5'
        try:
            testo[1]
            mor(msg)
        except:
            message = "Inviami ora la frase che vuoi trascrivere in *alfabeto latino*."
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/myid":
        message = '`'+str(chat_id)+'`'
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/globalmessage" and chat_id == prp:
        ram[chat_id] = '3'
        bot.sendMessage(chat_id,'Invia ora il messaggio da mandare a tutti gli utenti','Markdown',None,True,mess_id)

    elif command == "/enigma":
        ram[chat_id] = '6'
        testo = testostart
        try:
            testo[1]
            testo[2]
            testo[3]
            testo[4]
            mor(msg)
        except:
            message = "*Versione digitale della macchina enigma.*\n\nPer programmare la macchina devi inviarmi un messaggio indicandomi i rotori che intendi usare e la parola segreta. I rotori sono tre, numerati da uno a cinque in numeri romani. La parola segreta è composta da tre lettere.\n\nEsempio: `I V III MLB`"
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)
        
    elif command == "/toc":
        nr = random.randint(1,2)
        if nr == 1:
            bot.sendMessage(chat_id,"Testa",'Markdown',None,True,mess_id)
        if nr == 2:
            bot.sendMessage(chat_id,"Croce",'Markdown',None,True,mess_id)

    elif command == "/pass":
        psw = '`'
        for z in range(14):
            k = random.randrange(5)
            #Genera un numero casuale da 0 a 5
            if k < 4:
                j = random.randrange(50)+1
                psw = psw+string.ascii_letters[j]
            if k == 4 or k == 5:
                j = random.randrange(9)
                psw = psw+string.digits[j]
        psw += '`'
        bot.sendMessage(chat_id, psw,'Markdown', None, True, mess_id)

    elif command == '/spass':
        try:
            n = abs(int(testo[1]))
            if n == 0 or n>100:
                bot.sendMessage(chat_id,'La lunghezza inserita non è valida; sono generate password da 1 a 100 caratteri.','Markdown',None,True,mess_id)
                n = 14
        except:
            n = 14
        special = '!"#$%&\'()*+,-./:;<=>?@[\\]^_{|}~'
        caratteri = string.ascii_letters+string.digits+special
        psw = '`'
        for x in range(n):
            psw += caratteri[random.randint(0,len(caratteri)-1)]
        psw += '`'
        bot.sendMessage(chat_id, psw,'Markdown', None, True, mess_id)
    
    elif command == '/roll':
        try:
            facce = int(testo[2])
            if facce < 0:
                facce = facce*-1
            if facce == 0:
                facce = 6
            if facce > 100:
                bot.sendMessage(chat_id,"Spiacente, posso lanciare dadi solo fino a 100 facce",None, None, True, mess_id)
                facce = 6
        except:
            facce = 6
        try:
            dadi = int(testo[1])
            if dadi < 0:
                dadi = dadi*-1
            if dadi < 1:
                dadi = 1
            if dadi > 1000:
                bot.sendMessage(chat_id,"Il numero massimo di dadi lanciabili è 1000",None, None, True, mess_id)
                dadi = 1000
        except:
            dadi = 1
        somma = 0
        for x in range(dadi):
            somma += random.randint(1,facce)
        bot.sendMessage(chat_id,somma,None, None, True, mess_id)

    elif command == "/stats":
        message = 'Attualmente si sono registrati al bot '+str(len(elimina_lista_minori(list(ram.keys()))))+' utenti.\nNumero di messaggi inviati e ricevuti: '+str(mess_id)+"\n Ping ritorno: "+str(round((time.time()-tim0)*1000,5))
        bot.sendMessage(chat_id,message,None, None, True, mess_id)

    elif command == "/eliminami":
        del ram[chat_id]
        message = 'Sei stato rimosso con successo dagli archivi del bot.'
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/postlink":
        ram[chat_id] = '8'
        message = "*Inoltrami* ora il post di cui vuoi ottenere il link.\nRicorda che il canale deve avere un username!"
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/binario":
        ram[chat_id] = '9'
        message = "Mandami ora il *numero* che vuoi convertire in codice binario."  
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/short":
        ram[chat_id] = '10'
        try:
            testo[1]
            mor(msg)
        except:
            bot.sendMessage(chat_id,'Inviami il *link* che desideri accorciare','Markdown',None,True,mess_id)

    elif command == "/download": #Invia i file sorgenti del bot
        bot.sendChatAction(chat_id,'upload_document')
        files = ['Bot.py','techlib.py','upload/CONFIG.py'] #Si faccia attenzione a specificare bene i file
        for x in range(len(files)):
            bot.sendDocument(chat_id,open(pos+files[x],'rb'))

    elif command == "/time":
        ram[chat_id] = '11'
        message = "*Inoltrami* ora il messaggio di cui vuoi sapere la data originale di invio."
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/infomex":
        ram[chat_id] = '12'
        message = "*Inviami* ora il messaggio di cui vuoi ottenere tutte le informazioni.\nRiceverai indietro l'oggetto JSON dato da Telegram.\n\nSi consiglia l'uso di questo comando solo a chi interessato, in quanto poco fruibile per un utente medio."
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/upload":
        ram[chat_id] = '13'
        message = "*Inviami* ora il *file* che intendi caricare.\nVi forniremo un link facilmente condivisibile collegato al file.\nTutti i file sono conservati sui server di Telegram."
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/conta":
        ram[chat_id] = '14'
        message = '*Inviami* ora un messaggio per sapere da quanti caratteri è composto.'
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif command == "/fdw":
        tipo = parole(testo[1],'-')[1]
        dizionario = {'st':'sticker','do':'document','fo':'photo','ad':'audio','vo':'voice','vn':'video_note','vd':'video'}
        tipo = dizionario[tipo]
        file_id = ''
        for x in range(len(parole(testo[1],'-'))-2):
            x += 2
            file_id += parole(testo[1],'-')[x]+'-'
        file_id = elimina(file_id,-1)            
        if tipo == "sticker":
            bot.sendSticker(chat_id,file_id)
        elif tipo == "document":
            bot.sendDocument(chat_id,file_id)
        elif tipo == "photo":
            try:
                bot.sendMediaGroup(chat_id,file_id)
            except:
                bot.sendPhoto(chat_id,file_id)
        elif tipo == "audio":
            bot.sendAudio(chat_id,file_id)
        elif tipo == "voice":
            bot.sendVoice(chat_id,file_id)
        elif tipo == "video_note":
            bot.sendVideoNote(chat_id,file_id)
        elif tipo == "video":
            bot.sendVideo(chat_id,file_id)
        else:
            bot.sendMessage(chat_id,"⚠️ *IMPOSSIBILE RECUPERARE IL CONTENUTO*",'Markdown',None,True,mess_id)
        
    else: #Non è stato attivato alcun comando generico
        if imor != '0': #L'utente aveva un valore associato da un precedente comando
            mor(msg) #Apre la sezione dei sottomenù 

    carica(ssd,ram)
    
def mor(msg):
    global imor,chat_id,mess_id,imop,ram,testo,fields
    
    if imor == '0': 
        imor = parole(ram[chat_id])[0]
        ram[chat_id] = '0' #Se si è arrivati qui dopo aver attivato un comando, controlla se sia stato modificato il valore
    
    if imor == '1': #GetID
        if 'forward_date' in fields:
            if 'forward_from' in fields: 
                message = '`'+str(msg["forward_from"]["id"])+'`'
            else:
                message = '`'+str(msg["forward_from_chat"]["id"])+'`'
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)
        else:
            bot.sendMessage(chat_id,"Un errore mi ha impedito di recuperare l'ID dell'altro utente.\nAssicurati di *inoltrare* il messaggio!",'Markdown')

    elif imor == '2': #Traduzione in morse
        if testo[0] == "/morse":
            mex = ''
            for x in range(len(testo)-1):
                x += 1
                mex += testo[x]+" "
        else:
            mex = msg['text'].lower()
        ou = '`'
        z = 0
        dic={" ":"/","a":".-","b":"-...","c":"-.-.","d":"-..","e":".","f":"..-.","g":"--.","h":"....","i":"..","j":".---","k":"-.-","l":".-..","m":"--","n":"-.","o":"---","p":".--.","q":"--.-","r":".-.","s":"...","t":"-","u":"..-","v":"...-","w":".--","x":"-..-","y":"-.--","z":"--..","0":"-----","1":".----","2":"..---","3":"...--","4":"....-","5":".....","6":"-....","7":"--...","8":"---..","9":"----.",".":".-.-.-",",":"--..--",":":"---...","?":"..--..","=":"-...-","-":"-....-","(":"-.--.",")":"-.--.-",'"':".-..-.","'":".----.","/":"-..-.","@":".--.-.","!":"-.-.--"}
        #Costruzione del dizionario
        k=len(mex)
        for x in range(k):
            try:
                b = mex[z]
                z = z+1
                if b not in string.whitespace:
                    ou += '/'+dic[b]
                else:
                    ou += dic[b]
            except:
                None
        ou += "//`"
        message = 'Il codice morse relativo è:\n'+ou+"\nPer convertire dal morse all'alfabeto latino usa /morsetrad."
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '3': #Messaggio globale
        users = list(ram.keys())
        for x in range(len(users)):
            code = users[x]
            if 'text' in fields:
                bot.sendMessage(code, msg['text'],'Markdown')
                
    elif imor == '4': #Recensione
        if 'data' not in fields:
            bot.forwardMessage(prp,chat_id,mess_id)
            message = "Grazie mille per la tua recensione!"
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '5': #Traduzione dal morse
        if testo[0] == "/morsetrad":
            mex = testo[1]
        else:
            mex = msg['text'].lower()
        ou = '`'
        dic={" ":"/","a":".-","b":"-...","c":"-.-.","d":"-..","e":".","f":"..-.","g":"--.","h":"....","i":"..","j":".---","k":"-.-","l":".-..","m":"--","n":"-.","o":"---","p":".--.","q":"--.-","r":".-.","s":"...","t":"-","u":"..-","v":"...-","w":".--","x":"-..-","y":"-.--","z":"--..","0":"-----","1":".----","2":"..---","3":"...--","4":"....-","5":".....","6":"-....","7":"--...","8":"---..","9":"----.",".":".-.-.-",",":"--..--",":":"---...","?":"..--..","=":"-...-","-":"-....-","(":"-.--.",")":"-.--.-",'"':".-..-.","'":".----.","/":"-..-.","@":".--.-.","!":"-.-.--"}
        inversedic = dict((v, k) for (k, v) in dic.items()) #Costruzione del dizionario
        k=len(mex)
        c = ''
        ws = 0
        for x in range(k):
            try:
                b = mex[x]
                if b != '/':
                    c += b
                    ws = 0
                if b == "/":
                    ws += 1
                    if ws == 2:
                        ou += ' '
                    ou += inversedic[c]
                    c = ''
            except:
                None
        ou += '`'
        message = 'Il codice trascritto è: '+ou+"\nPer convertire dall'alfabeto latino al morse usa /morse"
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '6': #Raccolta dati della macchina enigma
        if testo[0] == "/enigma":
            del testo[0]
            mex = testo
        else:
            mex = msg['text'].lower()  
        rot_val = ['I','II','III','IV','V']
        #I rotori disponibil
        if testo[0] in rot_val and testo[1] in rot_val and testo[2]:
            rotori = testo[0]+' '+testo[1]+' '+testo[2]
            try:
                if len(testo[3]) == 3:
                    ram[chat_id] = str('7 '+rotori+' '+testo[3])
                    message = 'Macchina configurata. Inviami ora il testo che vuoi crittografare.'
                else:
                    message = 'Spiacente, non ho riconosciuto il codice.'
            except:
                message = 'Spiacente, non ho riconosciuto il codice.'
        else:
            message = 'Spiacente, non ho riconosciuto i rotori.'
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '7': #Creazione della macchina enigma e processione del testo
        rotori = imop[1]+' '+imop[2]+' '+imop[3]
        code = imop[4]
        machine = EnigmaMachine.from_key_sheet(rotors = rotori)
        machine.process_text(code)
        mex = msg['text'].lower()
        codice = machine.process_text(mex)
        link = botlink+'enigma-'+sostituisci(rotori,' ','-')+'-'+code
        message = "Il codice ottenuto è: `"+str(codice)+"`\n\nPuoi decifrarlo usando le medesime impostazioni della macchina.\n\nPer riutilizzare le impostazioni o condividerle con altri puoi usare il link: "+link
        bot.sendMessage(chat_id,message,'Markdown',True,None,mess_id)

    elif imor == '8': #Postlink
        try:
            username = msg['forward_from_chat']['username']
            try:
                ID = msg['forward_from_message_id']
                link = 't.me/'+str(username)+"/"+str(ID)
                message = 'Il link del post è: '+link+"\n\nClicca per copiarlo: `"+link+"`"
            except:
                message = 'Scusami, non sono riuscito ad ottenere il link'
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)
        except:
            message = 'Errore: username non trovato'
            bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '9': #Binario
        try:
            message = int(testo[0])
            message = elimina(bin(message),2)
            message = 'Il codice binario relativo è: '+message
        except: 
            message = 'Scusami, non sono riuscito a convertire il codice in binario'
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '10': #Shortlink
        if testo[0] == "/short":
            link = testo[1]
        else:
            link = testo[0]
        acc = ["http://","https://"]
        s = Shortener(Shorteners.BITLY, bitly_token=short_api)
        if esclusioni(link,acc) == False:
            link = "http://"+link
        try:
            message = 'Il link accorciato è: '+s.short(link)
        except Exception as e:
            message = 'Spiacente, non sono riuscito ad accorciare il link.\nSe il link è corretto potrebbe trattarsi di un problema ai server. Si prega di riprovare più tardi.'
            
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '11': #Time
        if 'forward_date' in fields:
            message = elimina(time.ctime(msg['forward_date']),3)  #Recupera l'orario del messaggio in tempo UNIX e lo trasforma in una data
        else:
            message = "Spiacente, non sono riuscito a recuperare la data.\nAssicurati di *inoltrare* il messaggio."
        bot.sendMessage(chat_id,message,'Markdown',None,True,mess_id)

    elif imor == '12': #Info complete
        bot.sendMessage(chat_id,pprint.pformat(msg),None,None,True,mess_id)

    elif imor == '13': #File upload
        try:
            tipo = list(msg.keys())[-1]
            dizionario = {'sticker':'st','document':'do','photo':'fo','audio':'ad','voice':'vo','video_note':'vn','video':'vd'}
            tipo = dizionario[tipo]
            try:
                code = msg[list(msg.keys())[-1]]['file_id']
            except:
                try:
                    code = msg[list(msg.keys())[-1]][0]['file_id']
                except:
                    code = msg[list(msg.keys())[-1]]['file_id'][0]
            link = botlink+'fdw-'+tipo+'-'+code
            message = "Puoi condividere il file usando questo link: "+link
            bot.sendMessage(chat_id,message,None,True,True,mess_id)
        except:
            bot.sendMessage(chat_id,'Errore. File non riconosciuto.','Markdown',None,True,mess_id)

    elif imor == '14':
        try:
            bot.sendMessage(chat_id,str(len(msg['text'])))
        except:
            bot.sendMessage(chat_id,"C'è stato un errore :(")
        
    carica(ssd,ram)

antiflood = {}
MessageLoop(bot,filtro).run_as_thread() #Riceve i messaggi

while 1: #Tiene attivo il bot
    antiflood = {} #Resetta ogni 5 secondi il filtro antiflood
    time.sleep(5)
