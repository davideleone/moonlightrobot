# -*- coding: utf-8 -*-
#Programmato da Davide Leone in Python 3 - Per contatti: leonedavide[at]protonmail.com
#Distribuito sotto licenza GNU Affero - Copyright 2018 Davide Leone
#Per MoonBot V3.4

"""File di configurazione per MoonBot.
    Copyright (C) 2018  Davide Leone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

#Import delle varie librerie
import time
import string
import random
import telepot
from telepot.loop import MessageLoop
import os
import pickle
from enigma.machine import EnigmaMachine
import pprint
from emoji import emojize
from pyshorteners import Shortener, Shorteners

username = '' #Username del bot
short_api = '' #goo.gl API
bot = telepot.Bot('') #Token del bot
prp = 0 #ID del proprietario
pos = "" #percorso del file all'interno del computer
ssd = pos+'moonbotramsystem.sav' #Sistema salvataggio dei dati
botlink = 'http://t.me/'+username+'?start='
